
export class Task {
    TaskId:number;
    ParentTask:number;
    TaskName:string;
    Priority:number;
    StartDate:Date;
    StartDateFormatted:Date;
    EndDateFormatted:Date;
    EndDate:Date;
    Flag:number;

}


