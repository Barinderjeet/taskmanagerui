import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AddComponent } from './ui/add/add.component';
import { ViewComponent } from './ui/view/view.component';
import { UpdateComponent } from './ui/update/update.component';
import { PipePipe } from './Pipes/pipe.pipe';
import {RouterModule} from '@angular/router';
import {AppRoutes}  from './app.routing.module';
import { ServiceService } from 'src/app/services/service.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TaskFilterPipe } from 'src/app/ui/view/TaskFilter';
import { CommonModule} from "@angular/common";
@NgModule({
  declarations: [
    AppComponent,
    AddComponent,
    ViewComponent,
    UpdateComponent,
    PipePipe,
   TaskFilterPipe
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
