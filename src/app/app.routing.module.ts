import { Component } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AddComponent} from 'src/app/ui/add/add.component';
import { ViewComponent } from 'src/app/ui/view/view.component';
import { NgModule } from '@angular/core';
import { UpdateComponent } from 'src/app/ui/update/update.component';

 


export const AppRoutes: Routes =[
{path: 'add',  component: AddComponent},
{path: 'view',  component: ViewComponent},
{ path: 'update/:TaskID', component: UpdateComponent }

];




