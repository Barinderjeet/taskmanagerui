import { Injectable } from '@angular/core';


import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import {Task} from 'src/app/task';
import { Observable } from 'rxjs';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { stringify } from '@angular/core/src/util';
//import { map } from 'rxjs/operators';
//import 'rxjs/add/operator/map';
//import 'rxjs/Rx';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  getColumns(): string[]{
    return ["TaskID","TaskName", "ParentTask", "Priority", "StartDate","EndDate"]};
    getTaskData():Observable<any>
    {
     return this.http.get("http://localhost/TaskManagerAPI/GetAll");
   /*
      let obs = this.http.get("http://localhost:64915/GetAll");
      obs.subscrwbbibe((response)=>console.log(response));
      return obs;*/
    }
    getByTaskId(TaskId:any):Observable<any>
    {
     return this.http.get("http://localhost/TaskManagerAPI/GetById?TaskId=" + TaskId);
 
    }
    postTaskData(TaskDetail:any):Observable<any>
    {
     console.log(TaskDetail);  
     return this.http.post("http://localhost/TaskManagerAPI/PostTask/",
     TaskDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    putTaskData(TaskDetail:any):Observable<any>
    {
     
     return this.http.put("http://localhost/TaskManagerAPI/UpdateTask/",
     TaskDetail); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
    
    deleteTaskData(TaskDetail:any):Observable<any>
    {
     console.log('INSIDE SERVICE' + JSON.stringify(TaskDetail) );
     console.log(JSON.stringify(TaskDetail));  
     return this.http.delete("http://localhost/TaskManagerAPI/DeleteTask?id=" + JSON.stringify(TaskDetail)
     
     
     ); 
     //.map(res => res );
     //.map((response:Response)=><any>response.json());
   
    }
   endTaskData(TaskDetail:any):Observable<any>
   {  
     
     return this.http.put("http://localhost/TaskManagerAPI/EndTask/" , TaskDetail);
     //.map(res=>res);
    
   }
 
}
