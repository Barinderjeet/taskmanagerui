import { Component, OnInit } from '@angular/core';
import { ServiceService  } from 'src/app/services/service.service';
import { NgModule } from '@angular/core';
import {Task} from 'src/app/task';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  taskDetails : any = {};
  constructor(private appservice:ServiceService) { }

  ngOnInit() {
  }

  add()
  {      
      
    console.log(this.taskDetails);
    this.appservice.postTaskData(this.taskDetails)    
    .subscribe(response=>
    {
      console.log(response);
      this.taskDetails = {};
      alert('Task Added!')      
    }
  );
 
      
      
  }
  reset()
  {

    
  }

}
