import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/services/service.service';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  taskDetails : any;
  taskID : any;

  constructor(private appSevice :ServiceService,private route : ActivatedRoute) {

    this.route.params.subscribe( params => { this.taskID = params.TaskID;
      console.log("this.taskID",params);
     });
   }

  ngOnInit() {
    this.appSevice.getByTaskId(this.taskID).subscribe( data => {
      console.log("data",data);
      this.taskDetails = data;
    }),error=>{
      console.log("error scenario");
    }

  }

  update(){
    this.appSevice.putTaskData(this.taskDetails).subscribe( data =>{
      console.log("success");
    })  
  }

}
