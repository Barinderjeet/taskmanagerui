//import { PipeTransform } from "@angular/core/src/change_detection/pipe_transform";
import { Task } from "src/app/task";
import { Pipe, PipeTransform } from '@angular/core';
import { NgAnalyzedFile } from "@angular/compiler";


@Pipe({

    name: "TaskFilter"
})
export class TaskFilterPipe implements PipeTransform{
    
    transform(View: Task[], taskName,parentTask,priorityFrom,priorityTo,startDate,endDate){
        if(!View || (!taskName && !parentTask && !priorityFrom && !priorityTo && !startDate && !endDate) )
        {
            return View;
        }

        return View.filter(task =>
            (taskName && task.TaskName.toLowerCase().
            indexOf(taskName.toLowerCase())  != -1)
        ||( parentTask && task.ParentTask.toString().
        indexOf(parentTask.toString())  != -1)
        ||( priorityFrom && task.Priority>
        priorityFrom)
        ||( priorityTo && task.Priority<
            priorityTo)
        ||( startDate && task.StartDate.toString().
            indexOf(startDate.toString())  != -1)
        ||( endDate && task.EndDate.toString().
            indexOf(endDate.toString())  != -1)
            
    );
    }
}