import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceService  } from 'src/app/services/service.service';
import { Router } from '@angular/router';
import {Task} from 'src/app/task';
import { SearchTask } from 'src/app/SearchTask';


@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  columns : any;
  TaskDetails : Task;
  
  
  parentTask:any;
    taskName:any;
    priorityFrom:any;
    priorityTo:any;
    startDate:any;    
    endDate:any;
    
  //searchTask : any = { "taskName" : "", "parentTask" : ""};
  searchTask1 : any;
 
  constructor(private appservice: ServiceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.columns =  this.appservice.getColumns();
    console.log(this.columns);
    this.appservice.getTaskData().subscribe
    (response=>
      {

      console.log("response" + JSON.stringify(response));
      
       this.TaskDetails = response;    
       
       console.log('kkk'+ this.TaskDetails.StartDate);
      }
      );
       
  
  }
editPage(col)
{
  console.log(col);
this.router.navigate(['update/' + col.TaskID]);
}


  deleteTask(col)
  {
    console.log("taskid",col.TaskID);
    this.appservice.deleteTaskData(col.TaskID)
    .subscribe(response=> {
      this.TaskDetails= response;
    });
  }

 endTask(col)
 {
   this.appservice.endTaskData(col)
   .subscribe(response=>{
     this.TaskDetails= response;
   });
 } 

}
